package com.garyhuang.falcon.domain.entities

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

sealed class NewsItem {
    @Parcelize
    data class Divider(val title: String) : Parcelable, NewsItem()
    @Parcelize
    data class News(val title: String, val sbuTitle: String, val subscript: String?,
                    val thumbnail: String?, val link: String?, val created: Date?) : Parcelable, NewsItem()
}
