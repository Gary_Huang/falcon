package com.garyhuang.falcon.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.garyhuang.falcon.R
import com.garyhuang.falcon.databinding.ItemDividerBinding
import com.garyhuang.falcon.databinding.ItemNewsBinding
import com.garyhuang.falcon.domain.entities.NewsItem
import com.garyhuang.falcon.network.GlideApp
import java.text.SimpleDateFormat
import java.util.*

class NewsAdapter(private val event: OnAdapterEvent): ListAdapter<NewsItem, RecyclerView.ViewHolder>(NewsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) DividerViewHolder(ItemDividerBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        else NewsViewHolder(ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(getItem(position)) {
            is NewsItem.Divider -> (holder as DividerViewHolder).bind(getItem(position) as NewsItem.Divider)
            is NewsItem.News -> (holder as NewsViewHolder).bind(event, getItem(position) as NewsItem.News)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(getItem(position)) {
            is NewsItem.Divider -> 0
            is NewsItem.News -> 1
        }
    }

    class DividerViewHolder(private val binding: ItemDividerBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(data: NewsItem.Divider) {
            binding.tvTitle.text = data.title
        }
    }

    class NewsViewHolder(private val binding: ItemNewsBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(event: OnAdapterEvent, data: NewsItem.News) {
            binding.root.setOnClickListener {
                event.onClickListener(data)
            }

            binding.tvTitle.text = data.title
            binding.tvSubtitle.text = data.sbuTitle

            val dateString: String? = data.created?.run {
                val format = SimpleDateFormat("MMM d, yyyy 'at' hh:mm aa", Locale.ENGLISH)
                format.format(this)
            }
            binding.tvCreated.text = dateString

            data.subscript?.run {
                binding.tvSubscript.text = this
                binding.tvSubscript.visibility = View.VISIBLE
            } ?: kotlin.run {
                binding.tvSubscript.visibility = View.GONE
            }

            data.thumbnail?.run {
                GlideApp.with(itemView)
                    .load(this)
                    .into(binding.iv)
            } ?: kotlin.run {
                binding.iv.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.black))
            }
        }
    }
}

class NewsDiffCallback : DiffUtil.ItemCallback<NewsItem>() {
    override fun areItemsTheSame(oldItem: NewsItem, newItem: NewsItem): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: NewsItem, newItem: NewsItem): Boolean = oldItem == newItem
}

interface OnAdapterEvent {
    fun onClickListener(data: NewsItem.News)
}