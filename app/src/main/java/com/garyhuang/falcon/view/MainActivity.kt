package com.garyhuang.falcon.view

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.garyhuang.falcon.databinding.ActivityMainBinding
import com.garyhuang.falcon.domain.entities.NewsItem
import com.garyhuang.falcon.network.Api
import com.garyhuang.falcon.network.data.toDomainNewsItemList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class MainActivity : AppCompatActivity(), OnAdapterEvent {
    private lateinit var binding: ActivityMainBinding
    private lateinit var newsAdapter: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        newsAdapter = NewsAdapter(this)
        binding.rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = newsAdapter

        binding.srl.setOnRefreshListener {
            getNews()
        }

        getNews()
    }

    private fun getNews() {
        binding.srl.isRefreshing = true
        lifecycleScope.launch(Dispatchers.Default) {
            try {
                val dateList = Api.instance.falconService.getNews().toDomainNewsItemList()
                withContext(Dispatchers.Main) {
                    newsAdapter.submitList(dateList)
                }
            }
            catch (e: Exception) {
                e.printStackTrace()
            }
            finally {
                binding.srl.isRefreshing = false
            }
        }
    }

    override fun onClickListener(data: NewsItem.News) {
        data.link?.run {
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
            val customTabsIntent: CustomTabsIntent = builder.build()
            customTabsIntent.launchUrl(this@MainActivity, Uri.parse(this))
        }
    }
}