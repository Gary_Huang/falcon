package com.garyhuang.falcon.network.service

import com.garyhuang.falcon.network.data.ApiNewsResponse
import com.garyhuang.falcon.network.endpoint.EndpointPath
import retrofit2.http.GET

interface FalconService {
    @GET(EndpointPath.News.PATH)
    suspend fun getNews(): ApiNewsResponse
}