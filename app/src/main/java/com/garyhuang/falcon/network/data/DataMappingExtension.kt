package com.garyhuang.falcon.network.data

import com.garyhuang.falcon.domain.entities.NewsItem
import java.util.*

fun ApiNewsResponse.toDomainNewsItemList(): List<NewsItem> {
    return this.vector.newsList.filter {
        it.type == "divider" || it.type == "news"
    }.map {
        if (it.type == "divider") NewsItem.Divider(it.title ?: "")
        else {
            it.appearance?.run {
                val date: Date? = it.extra?.created?.run {
                    Date(this)
                }
                NewsItem.News(mainTitle ?: "", subTitle ?: "", subscript, thumbnail, it.reference, date)
            } ?: NewsItem.Divider("")
        }
    }
}
