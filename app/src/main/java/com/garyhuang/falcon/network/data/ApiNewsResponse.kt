package com.garyhuang.falcon.network.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiNewsResponse(
    @field:Json(name = "getVector") val vector: Vector
)

@JsonClass(generateAdapter = true)
data class Vector(
    @field:Json(name = "items") val newsList: List<ApiNewsResponseInformation>
)

@JsonClass(generateAdapter = true)
data class ApiNewsResponseInformation(
    @field:Json(name = "type") val type: String,
    @field:Json(name = "title") val title: String?,
    @field:Json(name = "ref") val reference: String?,
    @field:Json(name = "appearance") val appearance: ApiNewsResponseAppearance?,
    @field:Json(name = "extra") val extra: ApiNewsResponseExtra?,
)

@JsonClass(generateAdapter = true)
data class ApiNewsResponseAppearance(
    @field:Json(name = "mainTitle") val mainTitle: String?,
    @field:Json(name = "subTitle") val subTitle: String?,
    @field:Json(name = "thumbnail") val thumbnail: String?,
    @field:Json(name = "subscript") val subscript: String?,
)

@JsonClass(generateAdapter = true)
data class ApiNewsResponseExtra(
    @field:Json(name = "created") val created: Long?,
)
