package com.garyhuang.falcon.network

import com.garyhuang.falcon.network.service.FalconService

class Api {
    private var _falconService: FalconService =
        RetrofitBuilder.createService(Host.FalconHost).create(FalconService::class.java)
    val falconService: FalconService
        get() = _falconService

    companion object {
        val instance by lazy {
            Api()
        }
    }
}