package com.garyhuang.falcon.network

import com.garyhuang.falcon.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.LoggingEventListener
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

sealed class Host {
    object FalconHost: Host()
}

object RetrofitBuilder {
    fun createService(host: Host): Retrofit = when(host) {
        Host.FalconHost -> createRetrofit(BuildConfig.HOST_FALCON)
    }

    private fun createRetrofit(host: String): Retrofit {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        return Retrofit.Builder()
            .baseUrl(host)
            .client(getOkHttpClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getOkHttpClient() =
        OkHttpClient.Builder()
            .connectTimeout(8, TimeUnit.SECONDS)
            .readTimeout(2, TimeUnit.SECONDS)
            .writeTimeout(2, TimeUnit.SECONDS)
            .also {
                if(BuildConfig.DEBUG && BuildConfig.HTTP_LOGGING) {
                    it.eventListenerFactory(LoggingEventListener.Factory())
                    it.addInterceptor(getHttpLoggingInterceptor()) }
            }.build()

    private fun getHttpLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY }

}